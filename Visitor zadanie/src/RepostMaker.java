
public class RepostMaker implements Visitor
{
	String report;
	int numberOfClients;
	int numberOfOrders;
	int numberOfProducts;
	

	public String showReport() 
	{
		return report;
	}
	public void setReport(String  report) 
	{
		this.report=report;
	}

	public	int getnumberOfClients()
	{
		return  numberOfClients;
	}
	public void setnumberOfClients(	int numberOfClients) 
	{
		this.numberOfClients=numberOfClients;
	}
	public	int getnumberOfOrders()
	{
		return numberOfOrders;
	}
	public void setnumberOfOrders(int numberOfOrders) 
	{
		this.numberOfOrders=numberOfOrders;
	}
	public	int getnumberOfProducts()
	{
		return numberOfProducts;
	}
	public void setnumberOfProducts(int numberOfProducts) 
	{
		this.numberOfProducts=numberOfProducts;
	}

	
	@Override
	public void visit(Visitable v) {
	
	}
}